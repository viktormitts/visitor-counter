# USAGE
# python multi_object_tracking_fast.py --prototxt mobilenet_ssd/MobileNetSSD_deploy.prototxt \
#	--model mobilenet_ssd/MobileNetSSD_deploy.caffemodel --video race.mp4

# import the necessary packages
from imutils.video import FPS
from imutils.video import VideoStream
import multiprocessing
import numpy as np
import argparse
import imutils
import dlib
import cv2
import os
import queue
from threading import Thread

def start_tracker(inputQueue, outputQueue, detectionQueue, n):
	# initialize the list of object trackers and corresponding class
	# labels
	trackers = []

	# loop indefinitely -- this function will be called as a daemon
	# process so we don't need to worry about joining it
	while True:
		# attempt to grab the next frame from the input queue
		rgb = inputQueue.get()

		if rgb is None:
			break

		# get new detections
		while not detectionQueue.empty():
			(i, box, label, oldRgb) = detectionQueue.get()

			print("[DEBUG] Tracker: Tracking new detection at " + str(box))

			# construct a dlib rectangle object from the bounding box
			# coordinates and then start the correlation tracker
			t = dlib.correlation_tracker()
			rect = dlib.rectangle(box[0], box[1], box[2], box[3])
			t.start_track(oldRgb, rect)
			trackers.append((i, t, label))
			ntrackers[n] += 1

		def update_trackers(i, t, label):
			t.update(rgb)
			pos = t.get_position()

			# unpack the position object
			startX = int(pos.left())
			startY = int(pos.top())
			endX = int(pos.right())
			endY = int(pos.bottom())

			# add the label + bounding box coordinates to the output
			# queue
			outputQueue.put((i, (label, (startX, startY, endX, endY))))

		# update the trackers and grab the position of the tracked
		# objects
		for (i, t, l) in trackers:
			Thread(target=update_trackers, args=(i, t, l)).start()

	while True:
		try:
			outputQueue.get(False)
		except queue.Empty:
			break
	print("[DEBUG] Tracker: Finish")

def new_detection(box, label, oldBoxes, rgb):
	if len(oldBoxes) > 0:
		print("[DEBUG] Main: Registered new detection at " + str(box) + " with trackers at")
		for ob in oldBoxes.values():
			print("[DEBUG] Main:	" + str(ob))
	else:
		print("[DEBUG] Main: Registered new detection at " + str(box))
	for (label, ebox) in oldBoxes.values():
		corners = []
		corners.append((box[0] - ebox[0], box[1] - ebox[1]))
		corners.append((box[2] - ebox[2], box[3] - ebox[3]))
		corners.append((corners[1][0], corners[0][1]))
		corners.append((corners[0][0], corners[1][1]))
		for corner in corners:
			if np.sqrt(corner[0]**2 + corner[1]**2) < NEW_DETECTION_THRESHOLD:
				return

	global count
	count += 1
	min = ntrackers[0]
	mini = 0
	for i in range(1,len(ntrackers)):
		if ntrackers[i] < min:
			min = ntrackers[i]
			mini = i
	detectionQueues[mini].put((count, box, label, rgb))
	boxes[count] = (label, box)

def start_detector(inputQueue, outputQueue):
	while True:
		print("[DEBUG] Detector: Waiting for input...")
		(frame, oldBoxes) = inputQueue.get()

		if frame is None:
			break

		# grab the frame dimensions and convert the frame to a blob
		(h, w) = frame.shape[:2]
		blob = cv2.dnn.blobFromImage(frame, 0.007843, (w, h), 127.5)

		if len(oldBoxes) > 0:
			print("[DEBUG] Detector: Detecting on frame with boxes:")
			for b in oldBoxes.values():
				print("[DEBUG] Detector:	" + str(b))
		else:
			print("[DEBUG] Detector: Detecting...")
		
		# pass the blob through the network and obtain the detections
		# and predictions
		net.setInput(blob)
		detections = net.forward()

		arranged = np.arange(0, detections.shape[2])
		people = 0

		# loop over the detections
		for i in arranged:
			# extract the confidence (i.e., probability) associated
			# with the prediction
			confidence = detections[0, 0, i, 2]

			# filter out weak detections by requiring a minimum
			# confidence
			if confidence > args["confidence"]:
				# extract the index of the class label from the
				# detections list
				idx = int(detections[0, 0, i, 1])
				label = CLASSES[idx]

				# if the class label is not a person, ignore it
				if CLASSES[idx] != "person":
					continue

				people += 1

				# compute the (x, y)-coordinates of the bounding box
				# for the object
				box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
				(startX, startY, endX, endY) = box.astype("int")
				bb = (startX, startY, endX, endY)

				print("[DEBUG] Detector: Detected person at " + str(bb))

				outputQueue.put((bb, label, oldBoxes, frame))

				print("[DEBUG] Detector: Put on detector output queue")

				# grab the corresponding class label for the detection
				# and draw the bounding box
				if showWindow:
					cv2.rectangle(frame, (startX, startY), (endX, endY),
						(0, 255, 0), 2)
					cv2.putText(frame, label, (startX, startY - 15),
						cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 255, 0), 2)

		print("[DEBUG] Detector: Done with detections")
		if people == 0:
			print("[DEBUG] Detector: No people detected")
			outputQueue.put(None)

	while True:
		try:
			outputQueue.get(False)
		except queue.Empty:
			break
	while True:
		try:
			inputQueue.get(False)
		except queue.Empty:
			break
	print("[DEBUG] Detector: Finish")

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--prototxt", required=True,
	help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", required=True,
	help="path to Caffe pre-trained model")
ap.add_argument("-i", "--input",
	help="path to input video file")
ap.add_argument("-o", "--output", type=str,
	help="path to optional output video file")
ap.add_argument("-c", "--confidence", type=float, default=0.4,
	help="minimum probability to filter weak detections")
ap.add_argument("-w", "--window", action='count',
	help="show window")
args = vars(ap.parse_args())

useInputFile = False
if args.get("input", False):
	useInputFile = True

showWindow = False
if args["window"] is not None:
	showWindow = True

# initialize our list of queues -- video input and output and detection
# queues for every tracker process
detectionInputQueue = multiprocessing.Queue()
detectionOutputQueue = multiprocessing.Queue()
detectionOutputQueue.put(None)
inputQueues = []
outputQueues = []
detectionQueues = []
ntrackers = []
processes = {}

count = 0

# initialize tracker processes (leave one processor for the
# detector and one for everything else)
for i in range(0,os.cpu_count()-2):
	iq = multiprocessing.Queue()
	oq = multiprocessing.Queue()
	dq = multiprocessing.Queue()
	inputQueues.append(iq)
	outputQueues.append(oq)
	detectionQueues.append(dq)
	ntrackers.append(0)
	p = multiprocessing.Process(
		target=start_tracker,
		args=(iq, oq, dq, i)
	)
	p.daemon = True
	p.start()
	processes["t" + str(i)] = p

# initialize the list of class labels MobileNet SSD was trained to
# detect
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
	"bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
	"dog", "horse", "motorbike", "person", "pottedplant", "sheep",
	"sofa", "train", "tvmonitor"]

NEW_DETECTION_THRESHOLD = 100

# load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

p = multiprocessing.Process(
	target=start_detector,
	args=(detectionInputQueue,detectionOutputQueue)
)
p.daemon = True
p.start()
processes["d"] = p

# initialize the video stream and output video writer
print("[INFO] starting video stream...")
if useInputFile:
	vs = cv2.VideoCapture(args["input"])
else:
	vs = VideoStream(src=0).start()
writer = None

# start the frames per second throughput estimator
fps = FPS().start()

boxes = {}

# loop over frames from the video file stream
while True:
	# grab the next frame from the video file
	frame = vs.read()
	if useInputFile:
		frame = frame[1]

	# check to see if we have reached the end of the video file
	if frame is None:
		break

	# resize the frame for faster processing and then convert the
	# frame from BGR to RGB ordering (dlib needs RGB ordering)
	frame = imutils.resize(frame, width=600)
	rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

	# if we are supposed to be writing a video to disk, initialize
	# the writer
	if args["output"] is not None and writer is None:
		fourcc = cv2.VideoWriter_fourcc(*"H264")
		writer = cv2.VideoWriter(args["output"], fourcc, 30,
			(frame.shape[1], frame.shape[0]), True)

	print("[DEBUG] Main: Handling detections...")
	detectorDone = False
	while True:
		try:
			got = detectionOutputQueue.get(False)
			if got is not None:
				(box, label, oldBoxes, oldFrame) = got
				print("[DEBUG] Main: Got detection at " + str(box))
				new_detection(box, label, oldBoxes, cv2.cvtColor(oldFrame, cv2.COLOR_BGR2RGB))
			detectorDone = True
		except queue.Empty:
			break
	if detectorDone:
		print("[DEBUG] Main: Giving the detector a new frame...")
		detectionInputQueue.put((frame, boxes))
	else:
		print("[DEBUG] Main: No output from detector")

	# loop over each of our input ques and add the input RGB
	# frame to it, enabling us to update each of the respective
	# object trackers running in separate processes
	for iq in inputQueues:
		while True:
			try:
				iq.get(False)
			except queue.Empty:
				break
		iq.put(rgb)
	
	# loop over each of the output queues
	for oq in outputQueues:
		# grab the updated bounding box coordinates for the
		# object -- the .get method is a blocking operation so
		# this will pause our execution until the respective
		# process finishes the tracking update
		while True:
			try:
				(tid, (label, (startX, startY, endX, endY))) = oq.get(False)
				boxes[tid] = (label, (startX, startY, endX, endY))
			except queue.Empty:
				break

	for (label, (startX, startY, endX, endY)) in boxes.values():
		# draw the bounding box from the correlation object
		# tracker
		if showWindow:
			cv2.rectangle(frame, (startX, startY), (endX, endY),
				(0, 255, 0), 2)
			cv2.putText(frame, label, (startX, startY - 15),
				cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
			cv2.putText(frame, "Count: " + str(count), (5, 15),
				cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 255), 2)

	# check to see if we should write the frame to disk
	if writer is not None:
		writer.write(frame)

	# show the output frame
	if showWindow:
		cv2.imshow("Frame", frame)
	key = cv2.waitKey(1) & 0xFF

	# if the `q` key was pressed, break from the loop
	if key == ord("q"):
		break
	
	# update the FPS counter
	fps.update()

# put None on the queues to tell the processes to stop
for q in inputQueues:
	q.put(None)
detectionInputQueue.put((None, {}))

# stop the timer and display FPS information
fps.stop()
print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# check to see if we need to release the video writer pointer
if writer is not None:
	writer.release()

# do a bit of cleanup
cv2.destroyAllWindows()
if useInputFile:
	vs.release()
else:
	vs.stop()

# wait for processes to finish
for p in processes.items():
	print("[DEBUG] Main: Waiting for " + p[0])
	p[1].join()
	p[1].close()